package com.example.englishgrammertest;

public class QuestionAndAnswer {
    String question_name;
    String correct_answer;

    public QuestionAndAnswer() {
    }

    public String getQuestion_name() {
        return question_name;
    }

    public void setQuestion_name(String question_name) {
        this.question_name = question_name;
    }

    public String getCorrect_answer() {
        return correct_answer;
    }

    public void setCorrect_answer(String correct_answer) {
        this.correct_answer = correct_answer;
    }
}